﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace Orderingg
{
    internal class Orderer
    {
        private const int size = 100;
        
        public void AddChar(List<char> charList)
        {
            Console.WriteLine("enter a char. ener 0 to exit.");
            char c;
            while ((c = Convert.ToChar(Console.ReadLine())) != '0')
            {
                int ctr = 0;
                while (ctr < charList.Count)
                {
                    if (charList[ctr] < c)
                    {
                        charList.Insert(ctr, c);
                        ctr = charList.Count;
                        break;
                    }
                    ctr++;
                }
                Console.WriteLine(charList);
            }
            
        }
    }
}
